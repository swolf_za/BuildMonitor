﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentScheduler;
using CacheManager.Core;

namespace BuildMonitor.BackgroundTasks
{
    public class TeamCityJobRegistry : Registry
    {
        public TeamCityJobRegistry()
        {
            Schedule<RecentBuildFetcher>().ToRunNow().AndEvery(2).Minutes();
            //Schedule<RecentBuildFetcher>().ToRunEvery(1).Minutes();
        }
    }
}
