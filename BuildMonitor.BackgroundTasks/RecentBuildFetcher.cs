﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentScheduler;
using CacheManager.Core;
using BuildMonitor.Queries;
using BuildMonitor.Data;
using System.Web.Hosting;

namespace BuildMonitor.BackgroundTasks
{
    public class RecentBuildFetcher : IJob, IRegisteredObject
    {
        private bool _shuttingDown;
        private readonly object _lock = new object();

        public void Execute()
        {
            lock (_lock)
            {
                if (_shuttingDown)
                    return;

                System.Diagnostics.Debug.Write("Fetching jobs");
                var buildQuries = new BuildQueries();

                Build[] builds = buildQuries.GetMostRecentBuilds(BuildMonitor.CacheManager.CacheManager.Cache);

                var key = BuildMonitor.CacheManager.CacheManager.BuildsCacheKey;
                if (BuildMonitor.CacheManager.CacheManager.Cache.Get(key) == null)
                {
                    BuildMonitor.CacheManager.CacheManager.Cache.Add(key, builds);
                }
                else
                {
                    BuildMonitor.CacheManager.CacheManager.Cache.Put(key, builds);
                }
            }
        }

        public void Stop(bool immediate)
        {
            // Locking here will wait for the lock in Execute to be released until this code can continue.
            lock (_lock)
            {
                _shuttingDown = true;
            }

            HostingEnvironment.UnregisterObject(this);
        }
    }
}
