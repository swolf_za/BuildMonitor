var gulp = require('gulp');
var gutil = require('gulp-util');
var debug = require('gulp-debug');
var template = require('gulp-template');
var runSequence = require('run-sequence');
var targetEnvironment = 'dev';

var requireDir = require('require-dir');
var dir = requireDir('./gulp tasks');
var environment = require('./environment.config.js');

gulp.task('setTargetEnvironment', function() {
    console.log('target environment ' + targetEnvironment);
    console.log(__dirname);
    gulp.src('./config.js')
        .pipe(debug())
        .pipe(template({environment: "'" + targetEnvironment + "'"}))
        .pipe(debug())
        .pipe(gulp.dest('./.build/js/app'))
        .pipe(debug());
});

gulp.task('live:build', function() {
    targetEnvironment = 'live';
    return runSequence('clean:js', 'setTargetEnvironment', 'dev:build');
});

gulp.task('qa:build', ['dev:build'], function() {
    targetEnvironment = 'qa';
    return runSequence('clean:js', 'setTargetEnvironment', 'dev:build');
});

gulp.task('default', function() {
    targetEnvironment = 'dev';
    return runSequence('clean:js', 'setTargetEnvironment', 'dev:build');
});
