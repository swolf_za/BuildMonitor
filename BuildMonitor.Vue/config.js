var config = (function () {
    'use strict';
    var environment = <%= environment %>;
    var enviroments = {
        dev : 'http://localhost:53011/',
        qa : 'http://buildmonitorlite.entelect.local/',
        live : 'http://esdashboard01.entelect.local/'
    };
    return {
        getBaseUrl : function () {
            return enviroments[environment];
        }
    };
})();
