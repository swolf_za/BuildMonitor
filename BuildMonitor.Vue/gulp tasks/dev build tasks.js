var path = require('path');
var es = require('event-stream');
var gulp = require('gulp');
var rename = require('gulp-rename');
var sass = require('gulp-sass');
var gutil = require('gulp-util');
var inject = require("gulp-inject");
var debug = require('gulp-debug');
var runSequence = require('run-sequence');
var flatten = require('gulp-flatten');
var preprocess = require('gulp-preprocess');
var order = require('gulp-order');

var environment = require('../environment.config.js');

gulp.task('dev:js:vendor', [], function () {

    return gulp.src(environment.vendorJsFiles)
        .pipe(gulp.dest(environment.buildDirectory + '/js/vendor'))
        .on('error', gutil.log);
});

gulp.task('dev:css:vendor', ['clean:css:vendor'], function () {
    return gulp.src(environment.vendorCssFiles)
        .pipe(gulp.dest(environment.buildDirectory + '/css/vendor'))
        .on('error', gutil.log);
});

gulp.task('dev:fonts', ['clean:fonts'], function () {
    return gulp.src(environment.fonts)
        .pipe(flatten())
        .pipe(gulp.dest(environment.buildDirectory + '/css/fonts'))
        .on('error', gutil.log);
});

gulp.task('dev:assets', ['clean:assets'], function () {
    return gulp.src(environment.assets)
        .pipe(flatten())
        .pipe(gulp.dest(environment.buildDirectory + '/assets'))
        .on('error', gutil.log);
});

gulp.task('dev:js', function () {
    var opts = {
        remove: true,
        add: true,
        single_quotes: true
    };
    return gulp.src([
        environment.sourceDirectory + 'buildStatusFactory.vue.js'//,
        //environment.sourceDirectory + 'config.js'
    ])
        .pipe(gulp.dest(environment.buildDirectory + '/js/app/'))
        .on('error', gutil.log);
});

gulp.task('dev:index', ['clean:html'], function () {
    var target = gulp.src('index.html');

    var vendorJSSources = gulp.src([environment.buildDirectory + '/js/vendor/**/*'])
        .pipe(order(environment.vendorJsOrder))
        .on('error', gutil.log);

    var vendorCSSSources = gulp.src([
        environment.buildDirectory + '/css/vendor/**/*',
        environment.buildDirectory + '/css/fonts/**/*',
        environment.distributionDirectory + '/fonts/**/*'
    ])
        .on('error', gutil.log);

    var sources = gulp.src([
        environment.buildDirectory + '/js/app/config.js',
        environment.buildDirectory + '/js/app/**/*.config.js',
        environment.buildDirectory + '/js/app/**/*',
        environment.buildDirectory + '/css/app/**/*'
    ])
    .pipe(debug())
    .pipe(order(["**/config.js", '*']))
        .on('error', gutil.log);

    return target
        .pipe(inject(
            es.merge(vendorJSSources, vendorCSSSources), {
                ignorePath: environment.buildDirectory.replace('./', ''),
                addRootSlash: false,
                name: 'vendor'
            }))
        .pipe(inject(sources, {
            ignorePath: environment.buildDirectory.replace('./', ''),
            addRootSlash: false
        }))
        .pipe(gulp.dest(environment.buildDirectory))
        .on('error', gutil.log);
});

// build task
gulp.task('dev:build', [], function (callback) {
    runSequence('dev:js:vendor', 'dev:css:vendor', 'dev:fonts', 'dev:assets', 'dev:js', 'dev:index', 'copy-build-to-dist', callback);
});
