setTimeout(function() {
	location.reload(true);
}, 6000000);

var buildList = function(element) {
	var baseUrl = config.getBaseUrl();

	var getBuildTiles = function(vue) {
		vue.getBuilds()
			.then(function(responses) {
				var tiles = responses.data.map(function(build) {
					var triggerDate = new Date(build.triggerDate).setHours(new Date(build.triggerDate).getHours()-2)
          build.triggerDate = moment(triggerDate).fromNow();

					return build;
				});
				var successfulBuild = tiles.filter(function(tile) {
					return tile.status === "SUCCESS";
				})
				vue.failedBuilds = tiles.filter(function(tile) {
					return tile.status !== "SUCCESS";
				})
				vue.successfulBuilds = successfulBuild;
			}).then(function() {
				vue.statusVisible = false;
			});
	};
	var getRunningBuildTiles = function(vue) {
		vue.getRunningBuilds()
			.then(function(responses) {
				vue.runningBuilds = responses.data;
				vue.quote = vue.getQuote();
			}).then(function() {
				vue.statusVisible = false;
			});
	};
	var getQueuedBuildTiles = function(vue) {
		vue.getQueuedBuilds()
			.then(function(responses) {
				vue.queuedBuilds = responses.data;
			}).then(function() {
				vue.statusVisible = false;
			});
	};
	var getCurrentAgents = function(vue) {
		vue.getAgents()
			.then(function(responses) {
				vue.agents = responses.data;
			}).then(function() {
				vue.statusVisible = false;
			});
	};
	return {
		el: element,
		data: {
			tiles: [],
			buildResponses: [],
			runningBuilds: [],
			builds: [],
			statusVisible: false,
			buildDictionary: {},
			failedBuilds: [],
			successfulBuilds: [],
			queuedBuilds: [],
			agents: [],
			quote: ""
		},
		mounted: function() {
			var refreshRate = 60000;

			getBuildTiles(this);
			var vue = this;
			setInterval(function() {
				getBuildTiles(vue);
			}, refreshRate);

			getRunningBuildTiles(this);
			setInterval(function() {
				getRunningBuildTiles(vue);
			}, refreshRate / 3);

			getQueuedBuildTiles(this);
			setInterval(function() {
				getQueuedBuildTiles(vue);
			}, refreshRate);

			getCurrentAgents(this);
		},
		methods: {
			getGlyphClass: function(tile) {
				if (tile.state == 'running') {
					return 'fa fa-hourglass-o fa-5x';
				} else if (tile.status == 'SUCCESS') {
					return 'fa fa-check fa-5x';
				} else if (tile.status == 'FAILURE') {
					return 'fa fa-exclamation fa-5x';
				}
				return 'fa fa-meh-o fa-5x';
			},
			getPanelClass: function(tile) {
				if (tile.state === 'running' || tile.state === 'queued') {
					return 'running label max-width';
				} else if (tile.status == 'SUCCESS') {
					return 'success label max-width entelect-success';
				} else if (tile.status == 'FAILURE') {
					return 'alert label max-width entelect-red';
				}
				return 'warning label max-width entelect-warning';
			},
			getBuilds: function() {
				return this.$http.get(baseUrl + 'api/builds/latest');
			},
			getRunningBuilds: function() {
				return this.$http.get(baseUrl + 'api/builds/running');
			},
			getQueuedBuilds: function() {
				return this.$http.get(baseUrl + 'api/builds/queued');
			},
			getAgents: function() {
				return this.$http.get(baseUrl + 'api/agents/current');
			},
			getAgentThermometerClass: function() {
				var loadPercentage = this.runningBuilds.length / this.agents.length;

				if (isNaN(loadPercentage) || loadPercentage < 0.2) {
					return 'fa fa-thermometer-empty';
				}
				if (loadPercentage < 0.4) {
					return 'fa fa-thermometer-quarter';
				}
				if (loadPercentage < 0.6) {
					return 'fa fa-thermometer-half';
				}
				if (loadPercentage < 0.8) {
					return 'fa fa-thermometer-three-quarters';
				}
				return 'fa fa-thermometer-full';
			},
			showQuote: function() {
				return this.runningBuilds.length < 1 && this.queuedBuilds.length < 1;
            },
            showRefactorMessage: function(){
                return this.failedBuilds.length < 1;
			},
			getQuote: function() {
				var quotes = [
					"Job done!", "Ready to work", "Work work...", "We need more lumber!", "You must construct additional pylons", "You require more Vespene Gas", "In the pipe, five by five", "Standin' by", "Prepped and ready", "CLEAR! (zap!)", "Orders, Cap'n?", "I can't build it, somethin's in the way", "I can't build there", "Job's finished", "Reportin' for duty", "Don't keep me waiting", "I am eager to help", "Something need doing?", "Ready to ride", "More work?", "Right-o", "Off I go, then!", "Yes, milord?", "Ready for action", "Locked and loaded!", "I await your command", "What's the flight plan?", "I'm on the job", "You're interrupting my calculations!", "That's not in the blueprints", "How did that get approved?", "How may I serve?"
				]

				var index = Math.floor(Math.random() * quotes.length);

				return quotes[index];
			}
		}
	};
};

new Vue(buildList('#buidList'));
new Vue(buildList('#incidentList'));
document.getElementById("incidentList").style.display="none";

var buildListShowing = true;

setInterval(function() {
  document.getElementById("buidList").style.display = buildListShowing ? "none" : "initial";
  document.getElementById("incidentList").style.display = buildListShowing ? "initial" : "none";
  buildListShowing = !buildListShowing;
}, 10000);