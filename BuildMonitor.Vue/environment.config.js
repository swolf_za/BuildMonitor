/// <reference path="vendor/jquery.gritter/js/jquery.gritter.js" />
/// <reference path="vendor/angular-cookies/angular-cookies.min.js" />
/**
 * This file/module contains all configuration for the build process.
 */

//your site URLs
var baseUrl = {
    dev: {
        serverName: 'silvertip.entelect.local',
        ssrsServerName: 'essqlreports/ReportServer_MSSQL2014'
    },
    qa: {
        serverName: 'fa-silvertip-qa.entelectprojects.co.za',
        ssrsServerName: 'essqlreports/ReportServer_MSSQL2014'
    },
    staging: {
        serverName: 'sv-faquotewebqa.supergrp.net',
        ssrsServerName: ''
    },
    live: {
        serverName: 'sv-faquoteweb1.supergrp.net',
        ssrsServerName: ''
    }
};


var config = {};

module.exports = {
    //the namespace of your Angular application
    appName: 'silvertip',

    //defualt build environment
    target: 'dev',

    //default debug flag
    debug: true,

    //
    silentFileRevisioning: true,

    //the port used by live reload during a gulp watch. Modify if you have more than one concurrent gulp watch
    liveReloadPort: 35729,

    //the app's source code directory
    sourceDirectory: './',

    //the app's intermediary build directory
    buildDirectory: './.build',

    //the app's common directory
    commonDirectory: '.src/modules/Silvertip.Angular.Common',

    //the app's API definitions directory. Only used when scaffolding with ng-scaffold
    apiSpecificationDirectory: './src/api',

    //bootstrap directory
    bootstrapDirectory: './bower_components/bootstrap-sass',

    //bootstrap style guide ouput directory
    styleguideDirectory: './styleguide',

    //the app's build output directory
    distributionDirectory: '../BuildMonitor/Vue', //supports relative pathing e.g. ../dist to place the distribution in the Angular project's parent directory

    //the url for app's API Swagger definition. Only used when scaffolding with ng-scaffold
    // swaggerAPIDocsUrl: 'http://silvertip.entelect.local:80/swagger/docs/v1',

    //explicit list of included vendor javascript dependencies
    vendorJsFiles: [
        'js/vendor/jquery.js',
        'js/vendor/what-input.js',
        'js/vendor/foundation.min.js',
        'js/app.js',
        'bower_components/moment/min/moment.min.js',
        'bower_components/vue/dist/vue.min.js',
        'bower_components/vue-resource/dist/vue-resource.min.js'
    ],

    //explicit set the rules for ordering vendor javascript files
    vendorJsOrder: [
        '**/jquery.js',
        '**/what-input.js',
        '**/foundation.min.js',
        '**/app.js',
        '**/moment.min.js',
        '**/vue.min.j',
        '**/vue-resource.min.js',
        '*'
    ],

    //explicit list of included vendor css dependencies
    vendorCssFiles: [
        'css/foundation.css',
        'css/app.css',
        'css/myriad.css',
        'list.css',
        'bower_components/components-font-awesome/css/font-awesome.min.css'
    ],

    //explicitly list any folders that you want to make available for @import in your sass code
    sassIncludeDirectories: [
        './bower_components/bootstrap-sass/assets/stylesheets',
        './src'
    ],

    //explicit list of included vendor font dependencies
    fonts: [
        'bower_components/components-font-awesome/fonts/*.*',
        'css/fonts/*.*'
    ],

    assets: [
        'assets/**/*'
    ],

    appConfig: {
        dev: {
            webapi: 'http://' + baseUrl.dev.serverName + '/api/',
            serverBase: 'http://' + baseUrl.dev.serverName + '/',
            ssrsBase: 'http://' + baseUrl.dev.ssrsServerName + '/',
            debug: true,
            environment: 'dev',
            //the url for app's API Swagger definition. Only used when scaffolding with ng-scaffold
            swaggerAPIDocsUrl: 'http://' + baseUrl.dev.serverName + '/swagger/docs/v1'
        },
        qa: {
            webapi: 'https://' + baseUrl.qa.serverName + '/api/',
            serverBase: 'https://' + baseUrl.qa.serverName + '/',
            ssrsBase: 'http://' + baseUrl.qa.ssrsServerName + '/',
            debug: true,
            environment: 'qa',
            //the url for app's API Swagger definition. Only used when scaffolding with ng-scaffold
            swaggerAPIDocsUrl: 'https://' + baseUrl.qa.serverName + '/swagger/docs/v1'
        },
        staging: {
            webapi: 'https://' + baseUrl.staging.serverName + '/api/',
            serverBase: 'https://' + baseUrl.staging.serverName + '/',
            ssrsBase: 'http://' + baseUrl.staging.ssrsServerName + '/',
            debug: true,
            environment: 'staging'
        },
        live: {
            webapi: 'https://' + baseUrl.live.serverName + '/api/',
            serverBase: 'https://' + baseUrl.live.serverName + '/',
            ssrsBase: 'http://' + baseUrl.live.ssrsServerName + '/',
            debug: true,
            environment: 'live'
        }
    }

};
