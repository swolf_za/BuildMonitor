﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuildMonitor.Data
{
    public class Build
    {
        public string WhoTriggered { get; set; }
        public DateTime TriggerDate { get; set; }
        public string BuildName { get; set; }
        public string Agent { get; set; }
        public string Project { get; set; }
        public string Name { get; set; }
        public int BuildCount { get; set; }
        public string State { get; set; }
        public string Status { get; set; }
        public string StatusText { get; set; }
        public int PercentageComplete { get; set; }
        public string BranchName { get; set; }
        public DateTime LastSuccessfulBuildDate { get; set; }
        public int DaysSinceLastIncident { get; set; }//aka days since last failed build
    }
}
