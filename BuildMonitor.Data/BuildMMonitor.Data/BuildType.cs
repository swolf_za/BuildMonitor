﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuildMonitor.Data
{
    public class  BuildType
    {
        public string id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public string projectName { get; set; }
        public string projectId { get; set; }
        public DateTime? lastSuccessfulBuildTimeUtc { get; set; }
    }
}
