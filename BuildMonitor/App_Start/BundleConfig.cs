﻿using System.Web;
using System.Web.Optimization;

namespace BuildMonitor
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new StyleBundle("~/Content/css").Include(
                        "~/Content/foundation.css",        
                        "~/Content/app.css",
                        "~/Content/list.css",
                        "~/Scripts/bower_components/components-font-awesome/css/font-awesome.css"
                        ));
            
            bundles.Add(new ScriptBundle("~/Scripts/scripts")
                .Include("~/Scripts/vendor/jquery.js")
                .Include("~/Scripts/vendor/what-input.js")
                .Include("~/Scripts/vendor/foundation.js")
                .Include("~/Scripts/app.js")
                .Include("~/Scripts/bower_components/moment/moment.js")
                .Include("~/Scripts/bower_components/crypto-js.js")
                .Include("~/Scripts/buildStatusFactory.vue.js")
                
            );
        }
    }
}