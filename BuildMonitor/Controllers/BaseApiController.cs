﻿using CacheManager.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace BuildMonitor.Controllers
{
    public class BaseApiController : ApiController
    {
        //private static ICacheManager<object> cache;
        public BaseApiController()
        {
            //if (cache == null)
            //{
            //    cache = CacheFactory.Build("getStartedCache", settings =>
            //    {
            //        settings.WithSystemRuntimeCacheHandle("handleName");
            //    });
            //}
        }


        public ICacheManager<object> Cache
        {
            get
            {
                return BuildMonitor.CacheManager.CacheManager.Cache;
            }
        }
    }
}