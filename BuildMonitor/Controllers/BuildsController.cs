﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BuildMonitor.Data;
using BuildMonitor.Queries;
using BuildMonitor.BackgroundTasks;
using FluentScheduler;

namespace BuildMonitor.Controllers
{
    public class BuildsController : BaseApiController
    {
        // GET api/values
        private const string BuildsCacheKey = "Builds";

        [HttpGet]
        [ActionName("Latest")]
        public IEnumerable<Build> GetLatest()
        {
            Build[] recentBuilds = Cache.Get<Build[]>(BuildsCacheKey);
            if (recentBuilds == null)
            {
                return new Build[0];
            }
            return recentBuilds;
        }

        [HttpGet]
        [ActionName("Running")]
        public IEnumerable<Build> GetRunning()
        {
            return new BuildQueries().GetRunningBuilds();
        }

        //[Route("Queued", Name = "GetQueued")]
        [HttpGet]
        [ActionName("Queued")]
        public IEnumerable<Build> GetQueued()
        {
            return new BuildQueries().GetQueuedBuilds();
            //https://teamcity.entelectprojects.co.za:8443/app/rest/latest/buildQueue
            //       <? xml version = "1.0" encoding = "UTF-8" standalone = "yes" ?>
            //< builds count = "1" href = "/app/rest/latest/buildQueue" >

            //       < build id = "258026" buildTypeId = "FleetAfricaSilvertip_Ci" state = "queued" branchName = "&lt;default&gt;" defaultBranch = "true" href = "/app/rest/latest/buildQueue/id:258026" webUrl = "https://teamcity.entelectprojects.co.za:8443/viewQueued.html?itemId=258026:0:17" />
            //                </ builds >
        }
    }
}