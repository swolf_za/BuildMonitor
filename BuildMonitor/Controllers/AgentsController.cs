﻿using System.Collections.Generic;
using System.Web.Http;
using BuildMonitor.Data;

namespace BuildMonitor.Controllers
{
    public class AgentsController : BaseApiController
    {
        // GET api/values
        private const string AgentsCacheKey = "Agents";

        [HttpGet]
        [ActionName("Current")]
        public IEnumerable<Agent> Current()
        {
            return new[]
            {
                new Agent
                {
                    Name = "ESBUILD-1"
                },
                new Agent
                {
                    Name = "ESBUILD-2"
                },
                new Agent
                {
                    Name = "ESBUILD-3"
                },
                new Agent
                {
                    Name = "ESBUILD-4"
                },
                new Agent
                {
                    Name = "ESBUILD-5"
                },
                new Agent
                {
                    Name = "ESBUILD-6"
                },
                new Agent
                {
                    Name = "ESBUILD-7"
                },
                new Agent
                {
                    Name = "ESBUILD-8"
                },
                new Agent
                {
                    Name = "ESBUILD-9"
                }
            };
            var currentAgents = Cache.Get<Agent[]>(AgentsCacheKey);
            if (currentAgents == null)
            {
                return new Agent[0];
            }
            return currentAgents;
        }
    }
}