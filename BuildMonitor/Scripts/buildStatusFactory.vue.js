var buildList = function() {
    var baseUrl = 'https://teamcity.entelectprojects.co.za:8443';//live
	//Vue.http.headers.common['Authorization'] = 'Basic ' + btoa('Projectstv:' + 'Enteradm1n');
    var getBuildTiles = function (vue) {
        vue.getBuilds()
            .then(function(responses) {
                    var tiles = responses.data.map(function (build) {
                        // build.triggerDate = moment(build.startDate).startOf('hour').fromNow();
                        build.triggerDate = moment(build.triggerDate).startOf('hour').fromNow();
                        return build;
                    });
                    vue.tiles = tiles;
            }).then(function() {
                vue.statusVisible = false;
            });
    };
	return {
		el: '#buidList',
		data: {
			tiles: [],
			buildResponses: [],
			runningBuilds: [],
			builds: [],
			statusVisible: false,
            buildDictionary: {},
            failedBuilds: []
		},
		mounted: function() {
            getBuildTiles(this);
            var vue = this;
            setInterval(function(){ getBuildTiles(vue); }, 60000);
		},
		methods: {
            getGlyphClass: function(tile) {
				if (tile.state == 'running') {
					return 'fa fa-hourglass-o fa-5x';
				} else if (tile.status == 'SUCCESS') {
					return 'fa fa-check fa-5x';
				} else if (tile.status == 'FAILURE') {
					return 'fa fa-exclamation fa-5x';
				}
				return 'fa fa-meh-o fa-5x';
			},
            getPanelClass: function(tile) {
				if (tile.status == 'SUCCESS') {
					return 'success label max-width';
				} else if (tile.status == 'FAILURE') {
					return 'alert label max-width';
				}

				return 'warning label max-width';
			},
			getBuilds: function() {
				return this.$http.get('http://localhost:404/api/builds');
			}
		}
	};
};

var app = new Vue(buildList());
