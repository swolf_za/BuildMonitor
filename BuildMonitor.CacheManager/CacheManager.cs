﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CacheManager.Core;

namespace BuildMonitor.CacheManager
{
    public static class CacheManager
    {
        public const string BuildsCacheKey = "Builds";
        public const string MostRecentBuildKey = "YoungestJob";

        private static ICacheManager<object> cache;
        public static ICacheManager<object> Cache
        {
            get
            {
                if (cache == null)
                {
                    cache = CacheFactory.Build("getStartedCache", settings =>
                    {
                        settings.WithSystemRuntimeCacheHandle("handleName");
                    });
                }
                return cache;
            }
        }
    }
}