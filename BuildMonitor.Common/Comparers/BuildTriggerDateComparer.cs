﻿using System.Collections.Generic;
using BuildMonitor.Data;

namespace BuildMonitor.Common.Comparers
{
    public class BuildTriggerDateComparer : IComparer<Build>
    {
        private readonly bool sortAscending;
        public BuildTriggerDateComparer(bool sortAscending = true)
        {
            this.sortAscending = sortAscending;
        }

        public int Compare(Build x, Build y)
        {
            //Sort descending
            int result = y.TriggerDate.CompareTo(x.TriggerDate);
            //Flip the sort of necassary
            result = sortAscending ? result*-1 : result;
            return result;
        }
    }
}
