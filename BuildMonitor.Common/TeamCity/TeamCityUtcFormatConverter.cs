﻿using System;

namespace BuildMonitor.Common.TeamCity
{
    public static class TeamCityUtcFormatConverter
    {
        public static DateTime ConvertTeamCityDateTimeToUtcDateTime(this string teamCityUtcDate)
        {
            string[] dateAndTime = teamCityUtcDate.Split('T');
            string date = dateAndTime[0];
            string time = dateAndTime[1];
            int year = Convert.ToInt32(date.Substring(0, 4));
            int month = Convert.ToInt32(date.Substring(4, 2));
            int day = Convert.ToInt32(date.Substring(6, 2));
            int hours = Convert.ToInt32(time.Substring(0, 2));
            int minutes = Convert.ToInt32(time.Substring(2, 2));
            int seconds = Convert.ToInt32(time.Substring(4, 2));

            int offsetHours = Convert.ToInt32(time.Substring(7, 2));
            int offsetMinutes = Convert.ToInt32(time.Substring(9, 2));

            var gmt = new DateTime(year
                , month
                , day
                , hours
                , minutes
                , seconds
                , System.DateTimeKind.Utc);

            return gmt;
        }

        public static string ConvertStringToWebFormattedTeamCityDateTime(this DateTime dateTime)
        {
            string year = dateTime.Year + "";
            string month = dateTime.Month > 10 ? dateTime.Month + "" : "0" + dateTime.Month;
            string day = dateTime.Day > 10 ? dateTime.Day + "" : "0" + dateTime.Day;
            string hour = dateTime.Hour > 10 ? dateTime.Hour + "" : "0" + dateTime.Hour;
            string minute = dateTime.Minute > 10 ? dateTime.Minute + "" : "0" + dateTime.Minute;
            var webFormattedDate = string.Format("{0}{1}{2}T{3}{4}00%2B0200", year, month, day, hour, minute);

            return webFormattedDate;
        }
    }
}
