﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuildMonitor.Common.TeamCity
{
    public static class UserNameEmailPartRemover
    {
        public static string RemoveEmail(this string username)
        {
            //chomp packages < martin@entelect.co.za >
            int emailStartIndex = username.IndexOf('<');
            if (emailStartIndex > -1)
            {
                username = username.Substring(0, emailStartIndex);
            }
            return username.Replace(" ", string.Empty);
        }
    }
}
