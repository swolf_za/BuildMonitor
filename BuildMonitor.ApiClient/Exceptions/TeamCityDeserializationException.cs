﻿using System;

namespace BuildMonitor.ApiClient.Exceptions
{
    public class TeamCityDeserializationException : Exception
    {
        public TeamCityDeserializationException()
            : base("Couldn't deserialize TeamCity's response.")
        {
            
        }
    }
}
