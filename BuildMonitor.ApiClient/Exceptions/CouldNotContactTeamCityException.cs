﻿using System;
using System.Net;

namespace BuildMonitor.ApiClient.Exceptions
{
    public class CouldNotContactTeamCityException : WebException
    {
        public CouldNotContactTeamCityException(Exception ex)
            : base("Couldn't contact TeamCity", ex)
        {

        }
        
        public CouldNotContactTeamCityException() : base("Couldn't contact TeamCity")
        {
            
        }
    }
}
