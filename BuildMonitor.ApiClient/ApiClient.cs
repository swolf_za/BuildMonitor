﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Configuration;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using BuildMonitor.Data;
using BuildMonitor.ApiClient.Exceptions;
using BuildMonitor.Common.Comparers;
using BuildMonitor.DataTransferObjects;
using BuildMonitor.Common.TeamCity;

namespace BuildMonitor.ApiClient
{
    public class ApiClient
    {
        private const string successStatus = "SUCCESS";
        private const string testFailuresStatus = "TESTSFAILED";
        private const string testsFailedStatusText = "Tests failed";
        private readonly string baseUrl;
        private const int dashboardTileLimit = 8;

        public ApiClient(string baseUrl = @"https://teamcity.entelectprojects.co.za:8443")
        {
            if (!baseUrl.Any())
            {
                throw new Exception("The TeamCity baseUrl is too short.");
            }
            if (baseUrl.EndsWith("/"))
            {
                baseUrl = baseUrl.Substring(0, baseUrl.Length - 1);
            }
            this.baseUrl = baseUrl;
        }

        private static HttpClient client;
        private HttpClient Client
        {
            get
            {
                if (client == null)
                {
                    client = new HttpClient();
                    client.BaseAddress = new Uri(baseUrl);

                    var credentials = Encoding.UTF8.GetBytes("Projectstv:Enteradm1n");
                    var token = Convert.ToBase64String(credentials);

                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", token);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                }
                return client;
            }
        }

        private struct BuildTypeResponse
        {
            public IEnumerable<BuildType> buildType;
        }

        public IEnumerable<BuildType> GetBuildTypes()
        {
            try
            {
                var request = Client.GetAsync(@"app/rest/latest/buildTypes?");
                var response = request.Result;

                if (response.IsSuccessStatusCode)
                {
                    var buildTypes = response.Content.ReadAsAsync<BuildTypeResponse>().Result;
                    return buildTypes.buildType;
                }
            }
            catch (AggregateException ex)
            {
                throw new CouldNotContactTeamCityException(ex);
            }
            catch (Exception ex)
            {
                throw new TeamCityDeserializationException();
            }
            throw new CouldNotContactTeamCityException();
        }

        private struct BuildTypeInfo
        {
            public int id { get; set; }
            public string status { get; set; }
            public string state { get; set; }
            public int percentageComplete { get; set; }
            public string buildTypeId { get; set; }
            public string branchName { get; set; }
        }

        private struct BuildTypeInfoResponse
        {
            public BuildTypeInfo[] build { get; set; }
        }

        public Build[] GetBuildsSinceTimeStamp(DateTime timeStamp)
        {
            string webFormattedDate = timeStamp.AddMinutes(-30).ConvertStringToWebFormattedTeamCityDateTime();
            var requestUri = string.Format(@"app/rest/latest/builds?locator=sinceDate:{0}", webFormattedDate);

            var sucessfulBuilds = new List<Build>();
            var failedBuilds = new List<Build>();
            var partiallyFailedBuilds = new List<Build>();

            var response = Client.GetAsync(requestUri).Result;
            if (response.IsSuccessStatusCode)
            {
                var buildInforesponse = response.Content.ReadAsAsync<BuildTypeInfoResponse>().Result;
                Parallel.ForEach(buildInforesponse.build, (buildInfo) =>
                {
                    string fetchBuildUri = string.Format("app/rest/latest/builds/id:{0}", buildInfo.id);
                    response = Client.GetAsync(fetchBuildUri).Result;
                    var buildInformation = response.Content.ReadAsAsync<BuildInformationDto>().Result;
                    var newBuild = buildInformation.ToBuild();
                    AddBuildToAppropriateList(newBuild, sucessfulBuilds, failedBuilds, partiallyFailedBuilds);
                });
            }

            return CombineAndSort(sucessfulBuilds, failedBuilds, partiallyFailedBuilds).Take(10).ToArray();
        }

        private void SetDaysSinceLastIncident(Build build, BuildType buildType)
        {
          if (build.Status == successStatus) {
            if (buildType.lastSuccessfulBuildTimeUtc == null) {
              buildType.lastSuccessfulBuildTimeUtc = build.TriggerDate;
            }
            build.DaysSinceLastIncident = (DateTime.Now - buildType.lastSuccessfulBuildTimeUtc.Value).Days;
          } else {
            buildType.lastSuccessfulBuildTimeUtc = null;
            build.DaysSinceLastIncident = 0;
          }
        }

        public Build[] GetLatestBuildsByBuildType(IEnumerable<BuildType> buildTypes)
        {
            var sucessfulBuilds = new List<Build>();
            var failedBuilds = new List<Build>();
            var partiallyFailedBuilds = new List<Build>();
            var cutOff = DateTime.UtcNow.AddDays(-7);

            Parallel.ForEach(buildTypes, (buildType) =>
            {
                var fetchBuildUri = string.Format(
                    "app/rest/latest/builds?locator=buildType:{0},start:0,count:1,branch:default:any&fields=build(id,status,state,buildType(name,id,projectName))"
                    , buildType.id);

                var response = Client.GetAsync(fetchBuildUri).Result;
                if (response.IsSuccessStatusCode)
                {
                    var buildTypeInfo = response.Content.ReadAsAsync<BuildTypeInfoResponse>().Result;

                    if (buildTypeInfo.build.Any())
                    {
                        fetchBuildUri = string.Format("app/rest/latest/builds/id:{0}", buildTypeInfo.build[0].id);
                        response = Client.GetAsync(fetchBuildUri).Result;
                        var buildInformation = response.Content.ReadAsAsync<BuildInformationDto>().Result;
                        var newBuild = buildInformation.ToBuild();
                        if (newBuild.TriggerDate >= cutOff)
                        {
                            SetDaysSinceLastIncident(newBuild, buildType);
                            AddBuildToAppropriateList(newBuild, sucessfulBuilds, failedBuilds, partiallyFailedBuilds);
                        }
                    }
                }
            });

            return CombineAndSort(sucessfulBuilds, failedBuilds, partiallyFailedBuilds).Take(dashboardTileLimit*2).ToArray();
        }

        public Build[] GetRunningBuilds()
        {
            List<Build> runnings = new List<Build>();
            var response = Client.GetAsync("app/rest/latest/builds?locator=running:true,branch:default:any").Result;
            if (!response.IsSuccessStatusCode)
            {
                return new Build[0];
            }
            var runningBuildInfo = response.Content.ReadAsAsync<BuildTypeInfoResponse>().Result;
            var runningBuilds = new List<Build>();
            if (runningBuildInfo.build == null || !runningBuildInfo.build.Any())
            {
                return runningBuilds.ToArray();
            }
            Parallel.ForEach(runningBuildInfo.build, (build) =>
            {
                response = Client.GetAsync(string.Format("app/rest/latest/builds/id:{0}", build.id)).Result;
                if (response.IsSuccessStatusCode)
                {
                    var buildInformation = response.Content.ReadAsAsync<BuildInformationDto>().Result;
                    var newBuild = buildInformation.ToBuild();
                    newBuild.PercentageComplete = build.percentageComplete;

                    runnings.Add(newBuild);
                }

            });
            return runnings.ToArray();
        }

        private List<Build> CombineAndSort(List<Build> sucessfulBuilds, List<Build> failedBuilds, List<Build> partiallyFailedBuilds)
        {
            var ascendingBuildSorter = new BuildTriggerDateComparer();
            failedBuilds.Sort(ascendingBuildSorter);
            partiallyFailedBuilds.Sort(ascendingBuildSorter);
            sucessfulBuilds.Sort(new BuildTriggerDateComparer(false));

            failedBuilds.AddRange(partiallyFailedBuilds);
            failedBuilds = failedBuilds.Take(dashboardTileLimit).ToList();
            failedBuilds.AddRange(sucessfulBuilds.Take(dashboardTileLimit));
            return failedBuilds;
        }

        private void AddBuildToAppropriateList(Build build, List<Build> successfulBuilds, List<Build> failedBuilds, List<Build> partiallyFailedBuilds)
        {
            if (build.Status != successStatus)
            {
                if (build.StatusText.Contains(testsFailedStatusText))
                {
                    build.Status = testFailuresStatus;
                    partiallyFailedBuilds.Add(build);
                }
                else
                {
                    failedBuilds.Add(build);
                }
            }
            else
            {
                successfulBuilds.Add(build);
            }
        }

        public Build[] GetQueuedBuilds()
        {
            List<Build> runnings = new List<Build>();
            var response = Client.GetAsync("/app/rest/latest/buildQueue").Result;
            if (!response.IsSuccessStatusCode)
            {
                return new Build[0];
            }
            var runningBuildInfo = response.Content.ReadAsAsync<BuildTypeInfoResponse>().Result;
            var runningBuilds = new List<Build>();
            if (runningBuildInfo.build == null || !runningBuildInfo.build.Any())
            {
                return runningBuilds.ToArray();
            }
            Parallel.ForEach(runningBuildInfo.build, (build) =>
            {
                response = Client.GetAsync(string.Format("app/rest/latest/builds/id:{0}", build.id)).Result;
                if (response.IsSuccessStatusCode)
                {
                    var buildInformation = response.Content.ReadAsAsync<BuildInformationDto>().Result;
                    var newBuild = buildInformation.ToBuild();
                    newBuild.PercentageComplete = build.percentageComplete;

                    runnings.Add(newBuild);
                }

            });
            return runnings.ToArray();
        }
    }
}
