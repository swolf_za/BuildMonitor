﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Security.Permissions;
using System.Text;
using System.Threading.Tasks;
using BuildMonitor.Data;
using CacheManager.Core;

namespace BuildMonitor.Queries
{
    public class BuildQueries
    {
        public const string BuildTypesKey = "BuildTypes";
        public Build[] GetMostRecentBuilds(ICacheManager<object> cache)
        {
            var client = new ApiClient.ApiClient();
            IEnumerable<BuildType> buildTypes = cache.Get<IEnumerable<BuildType>>(BuildTypesKey);
            if (buildTypes == null)
            {
                buildTypes = client.GetBuildTypes();
                cache.Add(BuildTypesKey, buildTypes);
            }
            
            return client.GetLatestBuildsByBuildType(buildTypes);
        }

        public Build[] GetRunningBuilds()
        {

            var client = new ApiClient.ApiClient();
            return client.GetRunningBuilds();
        }

        public Build[] GetQueuedBuilds()
        {
            var client = new ApiClient.ApiClient();
            return client.GetQueuedBuilds();
        }


    }
}
