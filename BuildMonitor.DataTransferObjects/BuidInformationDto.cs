﻿using BuildMonitor.Data;
using BuildMonitor.Common.TeamCity;

namespace BuildMonitor.DataTransferObjects
{
    public class BuildInformationDto
    {
        public int id { get; set; }
        public string buildTypeId { get; set; }
        public string number { get; set; }
        public string status { get; set; }
        public string state { get; set; }
        public string statusText { get; set; }
        public string queuedDate { get; set; }
        public string startDate { get; set; }
        public string finishDate { get; set; }
        public LastChangesDto lastChanges { get; set; }
        public BuildAgentDto agent { get; set; }
        public BuildTypeDto buildType { get; set; }
        public string branchName { get; set; }

        public Build ToBuild()
        {
            return new Build
            {
                Agent = agent != null ? agent.name : "Unknown",
                BuildName = buildType.name,
                Project = buildType.projectName,
                State = state,
                Status = status,
                TriggerDate = queuedDate.ConvertTeamCityDateTimeToUtcDateTime(),
                WhoTriggered = lastChanges != null
                                    ? lastChanges.change[0].username.RemoveEmail()
                                    : "Unknown",
                StatusText = statusText,
                BranchName = branchName
            };
        }
    }
}
