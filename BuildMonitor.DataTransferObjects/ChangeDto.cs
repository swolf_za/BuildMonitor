﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuildMonitor.DataTransferObjects
{
    public class ChangeDto
    {
        public int id { get; set; }
        public string username { get; set; }
        public string version { get;set; }
        public string date { get; set; } //at some point need to deal with the team city dates
    }
}
