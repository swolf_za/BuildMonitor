﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuildMonitor.DataTransferObjects
{
    public class LastChangesDto
    {
        public ChangeDto[] change { get; set; }
    }
}
