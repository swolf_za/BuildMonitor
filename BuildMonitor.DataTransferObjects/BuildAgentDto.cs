﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuildMonitor.DataTransferObjects
{
    public class BuildAgentDto
    {
        public int id { get; set; }
        public string name { get; set; }
    }
}
