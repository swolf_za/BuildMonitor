﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuildMonitor.DataTransferObjects
{
    public class BuildTypeDto
    {
        public string id { get; set; }
        public string name{ get; set; }
        public string projectName{ get; set; }
        public string projectId { get; set; }
    }
}
